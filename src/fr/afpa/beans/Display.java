package fr.afpa.beans;

import java.util.Scanner;

import fr.afpa.control.Control;

public class Display {

	Control control = new Control();
	DBManagement dbManage = new DBManagement();

	public void runSgbd() {
		showMenu();
	}

	public void showMenu() {
		System.out.println("Bienvenue sur la SGBD Java.");
		dbManage.createDatabaseFolders();
		boolean loginOk = false;
		while (!loginOk) {
			if (dbManage.authUser()) {
				loginOk = true;
			}
		}
		displayHelp();
	}

	/**
	 * Création d'un menu d'aide utilisateur en cas de saisie d'une commande
	 * inconnue Affichage de l'ensemble des commandes possibles (opérations CRUD)
	 * 
	 * @author Thomas
	 */
	public void displayHelp() {
		boolean menuRun = true;

		String menu = new String();
		menu = "\n";
		menu += "Voici les commandes disponibles pour interagir avec la base de données :\n";
		menu += "\n";
		menu += "CREATE TABLE nomTable(colonne1, colonne2, colonne3);\n";
		menu += "INSERT INTO nomTable VALUES('valeur1','valeur2');\n";
		menu += "SELECT * FROM nomTable;\n";
		menu += "SELECT nom_du_champ FROM nomTable;\n";
		menu += "SELECT * FROM nomTable OrderBy nom_du_champ ASC;\n";
		menu += "UPDATE nomTable SET nom_du_champ='nouvellevaleur';\n";
		menu += "UPDATE nomTable SET nom_du_champ='nouvellevaleur' WHERE nom_du_champ='anciennevaleur';\n";
		menu += "\n";
		menu += "N'oubliez pas les majuscules. Si vous entrez des valeurs, celles-ci ne peuvent dépasser 25 caractères.\n";
		menu += "Pour quitter, entrez 'quit'.\n";
		
		while (menuRun) {
		System.out.println(menu);
		
		askEntry();
		}
	}
	
	/**
	 * Demande à l'utilisateur d'entrer sa commande
	 * 
	 * @author Cécile
	 */
	public void askEntry() {
		System.out.println("Merci d'entrer une commande à effectuer :");
		System.out.print("SQL:>");
		Scanner in = new Scanner(System.in);
		String entry = in.nextLine();

		if (entry.equalsIgnoreCase("QUIT")) {
			System.exit(-1);
		} else {
			dbManage.callCategory(entry);
		}
	}
	
	
	/**
	 * Permet d'afficher une table à la suite d'un select all
	 * 
	 * @param table : la table qui sera affichée
	 * @author Cécile
	 */
	public void showSelectAll(Table table) {
		//I spent way too much time on this thing

		String format = new String();
		String formatTopBot = "+------";

		String[] columnSplitter = table.getColumn().split(";");
		int maxSize = 0;
		int dataCounter = 0;

		for (int i = 0; i < columnSplitter.length; i++) {
			if (columnSplitter[i].length() > maxSize) {
				maxSize = columnSplitter[i].length();
			}
			dataCounter++;
		}
		
		String[] valueSplitter = table.getValue().split("\\n");
		String[] value = null;
		
		for (int i = 0; i < valueSplitter.length; i++) {
			value = valueSplitter[i].split(";");

			for (int j = 0; j < value.length; j++) {
				if (value[j].length() > maxSize) {
					maxSize = value[j].length();
				}
			}
		}

		for (int i = 1; i <= dataCounter; i++) {
			for (int j = 0; j < maxSize; j++) {
				formatTopBot += "-";
			}
			format += "| %" + i + "$-" + (maxSize + 2) + "s";
		}
		
		format += " |\n";
		formatTopBot += "------+";
		
		System.out.println(formatTopBot);
		System.out.format(String.format(format, (Object[]) columnSplitter));
		System.out.println(formatTopBot);
		
		for (int i = 0; i < valueSplitter.length; i++) {
			value = valueSplitter[i].split(";");
			System.out.format(String.format(format, (Object[]) value));
		}
		System.out.println(formatTopBot);
	}
}
