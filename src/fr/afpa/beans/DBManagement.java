package fr.afpa.beans;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import fr.afpa.control.Control;

/**
 * Classe regroupant les méthodes permettant à la base de données de fonctionner
 * 
 * @author Cécile
 *
 */
public class DBManagement {

	private Control control = new Control();

	/**
	 * Méthode créant le répertoire et fichier config.ini nécessaires au
	 * fonctionnement de la base de données
	 * 
	 * @author Cécile
	 */
	public void createDatabaseFolders() {
		if ((createEnv() && createBdd()) || createBdd()) {
			System.out.println("Répertoire C:/ENV/BDD créé.");
		}
		try {
			if (createIni()) {
				System.out.println("Fichier config.ini créé.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Méthode créant le dossier ENV si celui-ci n'existe pas
	 * 
	 * @return booléenne pour confirmer la création du dossier
	 * @author Cécile
	 */
	private boolean createEnv() {
		if (!this.control.checkEnvExists()) {
			File dir = new File("C:/ENV");
			dir.mkdir();
			return true;
		}
		return false;
	}

	/**
	 * Méthode créant le dossier BDD si celui-ci n'existe pas
	 * 
	 * @return booléenne pour confirmer la création du dossier
	 * @author Cécile
	 */
	private boolean createBdd() {
		if (!this.control.checkBddExists()) {
			File dir = new File("C:/ENV/BDD");
			dir.mkdir();
			return true;
		}
		return false;
	}

	/**
	 * Méthode créant le fichier config.ini si celui-ci n'existe pas
	 * 
	 * @return booléenne pour confirmer la création du fichier
	 * @author Cécile
	 * @throws IOException
	 */
	private boolean createIni() throws IOException {
		if (!this.control.checkIniExists()) {
			FileWriter writer = new FileWriter("C:/ENV/BDD/config.ini");
			try {
				File ini = new File("C:/ENV/BDD/config.ini");
				ini.createNewFile();
				writer.write("admin/password");
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				writer.close();
			}
		}
		return false;
	}

	/**
	 * Vérification de l'identification de l'utilisateur afin d'accéder à la BDD
	 * Nécessite de lire le fichier config.ini qui comporte le couple login/mdp
	 * Appelle la méthode checkIdentity() de la classe Control
	 * 
	 * @return booléenne : indique si l'identification a pu se faire
	 * 
	 * @author Thomas, edité par Cécile
	 * 
	 */
	public boolean authUser() {

		System.out.println("Veuillez entrer votre identifiant :");
		Scanner in = new Scanner(System.in);
		String login = in.nextLine();

		System.out.println("Veuillez entrer votre mot de passe :");
		String password = in.nextLine();

		String logPass = login + "/" + password;

		String iniLogin = this.control.loginCollect();
		if (control.checkIdentity(logPass, iniLogin)) {
			return true;
		}
		return false;
	}

	/**
	 * Appèle la méthode correspondant au retour des vérifications de requêtes
	 * 
	 * @param query : la reuqête utilisateur qui sera examinée
	 * 
	 * @author Cécile
	 */
	public void callCategory(String query) {
		String answer = control.checkUserEntry(query);

		if (answer.equals("create")) {
			createTable(query);
		} else if (answer.equals("selectAll")) {
			selectAll(query);
		} else if (answer.equals("insertInto")) {
			insertInto(query);
		} else if (answer.equals("update")) {
			update(query);
		} else if (answer.equals("error")) {
			System.out.println("Erreur de syntaxe. Veuillez recommencer votre requête.");
		}
	}

	/**
	 * Méthode permettant de créer une table suivant la requête utilisateur
	 * 
	 * @param query : la requête à traiter
	 * 
	 * @author Cécile
	 */
	private void createTable(String query) {

		String tableName = control.captureCreateTableName(query);
		String columns = control.captureValues(query);
		columns = columns.replaceAll("[\\(\\);]", "");
		columns = columns.replaceAll(", ", ";");

		FileWriter writer = null;
		BufferedWriter buffer = null;
		File table = new File("C:/ENV/BDD/" + tableName + ".cda");

		if (!table.exists() && !table.isFile()) {
			try {
				table.createNewFile();
				writer = new FileWriter("C:/ENV/BDD/" + tableName + ".cda");
				buffer = new BufferedWriter(writer);

				buffer.write(columns);
				System.out.println("Table " + tableName + ".cda créée dans C:/ENV/BDD.");

			} catch (IOException e) {
				System.out.println("Erreur : " + e);
			} finally {
				if (buffer != null) {
					try {
						buffer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println(
					"Erreur : la table " + tableName + " existe déjà. Veuillez entrer un nom de table différent.");
		}
	}

	/**
	 * Méthode qui permet d'insérer des valeurs dans la table souhaitée
	 * 
	 * @author Thomas
	 * @param query = la requête envoyée par l'utilisateur
	 */
	public void insertInto(String query) {
		if (control.checkUserInsertInto(query) == true) {
			String tableName = control.captureInsertTableName(query);
			String data = control.captureInsertValues(query);
			FileWriter fw = null;
			BufferedWriter buffer = null;
			FileReader reader = null;
			BufferedReader br = null;
			// StringBuffer sb = new StringBuffer(100);
			String column = "";

			File table = new File("C:/ENV/BDD/" + tableName + ".cda");

			if (!table.exists()) {
				System.out.println("La table " + tableName + " n'existe pas");
			} else {
				data = data.replaceAll("[\\(\\)']", "");
				String[] dataSplitter = data.split(",");
				int dataCounter = 0;

				for (int i = 0; i < dataSplitter.length; i++) {
					dataCounter++;
				}
				System.out.println(dataCounter);
				data = data.replaceAll(",", ";");

				try {
					reader = new FileReader("C:/ENV/BDD/" + tableName + ".cda");
					br = new BufferedReader(reader);
					column = br.readLine();

					String[] columnTitles = column.split(";");
					int columnCounter = 0;
					for (int i = 0; i < columnTitles.length; i++) {
						columnCounter++;
					}
					System.out.println(columnCounter);
					if (columnCounter != dataCounter) {
						System.out.println("Le nombre de valeurs doit correspondre au nombre de colonnes de la table "
								+ tableName);
					} else {
						if ((br.readLine()).equals(null)) {
							FileWriter writer = new FileWriter("C:/ENV/BDD/" + tableName + ".cda", true);
							writer.write("\n");
							writer.write(data);
							writer.close();
							// Files.write(Paths.get("C:/ENV/BDD/" + tableName + ".cda"), data.getBytes(),
							// StandardOpenOption.APPEND);
						}
					}
				} catch (IOException e) {
					System.out.println("Erreur : " + e);
				} finally {
					if (buffer != null) {
						try {
							buffer.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Sélectionne toutes les données d'une table pour les afficher
	 * 
	 * @param query : la requête à traiter
	 * @return Table : retourne un objet table qui pourra être traité
	 */
	public Table selectAll(String query) {
		String tableName = control.captureSelectAllTableName(query);
		String column = "";
		String value = "";
		Table table = new Table();

		if (control.checkTableExists(tableName)) {
			FileReader reader = null;
			BufferedReader buffRead = null;
			int count = 0;
			try {
				reader = new FileReader("C:/ENV/BDD/" + tableName + ".cda");
				buffRead = new BufferedReader(reader);
				while (buffRead.ready()) {
					if (count == 0) {
						column += buffRead.readLine();
						table.setColumn(column);
						count++;
					} else {
						value += buffRead.readLine();
						value += "\n";
						table.setValue(value);
					}
				}
				return table;

			} catch (Exception e) {
				System.out.println("Erreur : " + e);
			} finally {
				if (buffRead != null) {
					try {
						buffRead.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("La table n'existe pas");
		}
		return table;
	}

	/**
	 * Permet de mettre à jour une table en remplaçant une valeur
	 * 
	 * @param query : la requête à traiter
	 * @author Thomas
	 */
	public void update(String query) {
		String tableName = control.captureUpdateTableName(query);
		String columnName = control.captureColumnName(query);
		String newValue = control.captureNewValue(query);

		File table = new File("C:/ENV/BDD/" + tableName + ".cda");

		if (control.checkTableExists(tableName)) {
			FileReader reader = null;
			BufferedReader buffRead = null;
			FileWriter writer = null;
			BufferedWriter buffer = null;

			try {
				reader = new FileReader("C:/ENV/BDD/" + tableName + ".cda");
				buffRead = new BufferedReader(reader);
				if ((buffRead.readLine()).equals(columnName)) {
					writer = new FileWriter("C:/ENV/BDD/" + tableName + ".cda");
					buffer = new BufferedWriter(writer);
					buffer.write(newValue);
				}
			} catch (IOException e) {
				System.out.println("Erreur : " + e);
			} finally {
				if (buffer != null) {
					try {
						buffer.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out.println("La table " + tableName + " n'existe pas. Veuillez entrer un nom de table valide.");
		}
	}

}