package fr.afpa.control;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe de contrôle, permet de vérifier diverses étapes du programme (création
 * de fichier, commande entrée par l'utilisateur...)
 * 
 * @author Cécile
 *
 */
public class Control {

	/**
	 * Permet de vérifier si le dossier ENV existe
	 * 
	 * @return booléenne pour confirmer l'existence ou non du dossier
	 * @author Cécile
	 */
	public boolean checkEnvExists() {
		File env = new File("C:/ENV");
		if (env.exists() && env.isDirectory()) {
			return true;
		}

		return false;
	}

	/**
	 * Permet de vérifier si le dossier BDD existe
	 * 
	 * @return booléenne pour confirmer l'existence ou non du dossier
	 * @author Cécile
	 */
	public boolean checkBddExists() {
		File bdd = new File("C:/ENV/BDD");
		if (bdd.exists() && bdd.isDirectory()) {
			return true;
		}
		return false;
	}

	/**
	 * Permet de vérifier si le fichier config.ini existe
	 * 
	 * @return booléenne pour confirmer l'existence ou non du fichier
	 * @author Cécile
	 */
	public boolean checkIniExists() {
		File ini = new File("C:/ENV/BDD/config.ini");
		if (ini.exists() && ini.isFile()) {
			return true;
		}
		return false;
	}

	/**
	 * Vérifie le type de requête qu'a voulu entrer l'utilisateur, avec une certaine
	 * marge d'erreur
	 * 
	 * @param entry : la requête utilisateur
	 * @author Cécile, édité par Thomas
	 */
	public String checkUserEntry(String query) {
		// Abandon all hope ye who enter here, for the regexes haunt this pit

		String category = new String();

		Pattern create = Pattern.compile("(?i)^CR[EZSDFR][AQSZ]TE T[AQSZ][BVGHN]LE(?-i)");
		Matcher createMatch = create.matcher(query);
		Pattern from = Pattern.compile("(?i)^F[REDFGT]OM(?-i)");
		Matcher matchFrom = from.matcher(query);

		Pattern insertInto = Pattern
				.compile("(?i)(^I[NBHJK]S[EZSDFR]RT I[NBHJK][TRFGHY]O)(?-i) ([\\w]+) (?i)(V[AQSZ]L[UYHJKI]ES)(?-i)");
		Matcher insertIntoMatch = insertInto.matcher(query);
		Pattern select = Pattern.compile("(?i)^S[EZSDFR]L[EZSDFR]CT(?-i)");
		Matcher selectMatch = select.matcher(query);
		Pattern selectAll = Pattern.compile("(?i)^S[EZSDFR]L[EZSDFR]CT \\* F[REDFGT][OIKLMP]M(?-i)");
		Matcher selectAllMatch = selectAll.matcher(query);

		Pattern updateSet = Pattern.compile("(?i)(^U[POLM%^ù]D[AQSZ]TE)(?-i) ([\\w]+) (?i)(S[EZSDFR]T)(?-i)");
		Matcher updateSetMatch = updateSet.matcher(query);

		while (createMatch.find()) {
			String match = createMatch.group(0);
			if (match.matches("CREATE TABLE")) {
				if (checkUserCreate(query)) {
					return category = "create";
				} else {
					return category = "error";
				}
			} else {
				System.out.println("Commande detectée mais syntaxe erronée. Vouliez-vous dire : CREATE TABLE ?\n");
			}
		}

		while (insertIntoMatch.find()) {
			String match = insertIntoMatch.group(1);
			String values = insertIntoMatch.group(3);
			
			if (match.matches("INSERT INTO") && values.matches("VALUES")) {
				if (checkUserInsert(query)) {
					return category = "insertInto";
				} else {
					return category = "error";
				}
			} else {
				System.out.println(
						"Commande detectée mais syntaxe erronée. Vouliez-vous dire : INSERT INTO nomDeTable VALUES ?");
			}
		}

		while (selectAllMatch.find()) {
			String match = selectAllMatch.group(0);
			System.out.println(match);
			if (match.matches("SELECT * FROM")) {
				if (checkUserSelectAll(query)) {
					return category = "selectAll";
				} else {
					return category = "error";
				}
			} else {
				System.out.println("Commande detectée mais syntaxe erronée. Vouliez-vous dire : SELECT * FROM ?");
			}
		}

		while (updateSetMatch.find()) {
			String match = updateSetMatch.group(1);
			String setM = updateSetMatch.group(3);
			
			if (match.matches("UPDATE") && setM.matches("SET")) {
				if (checkUserUpdateSet(query))
				return category = "update";
			} else {
				System.out.println("Commande detectée mais syntaxe erronée. Vouliez-vous dire : UPDATE nomDeTable SET ?");
			}
		}

		return category;
	}

	/**
	 * Vérification de la validité de la requête create.
	 * 
	 * @param query : la requête à vérifier
	 * @return booléenne : valide ou pas la requête écrite par l'utilisateur
	 * @author Cécile
	 */
	public boolean checkUserCreate(String query) {
		Pattern create = Pattern.compile("^(CREATE TABLE) ([\\w]+)\\([\\w ]{1,25}(, [\\w ]{0,25})*\\);$");
		Matcher createMatch = create.matcher(query);
		while (createMatch.find()) {
			return true;
		}
		return false;
	}
	
	/**
	 * @author Thomas
	 * Vérification de la validité de la requête "INSERT INTO" via la regex
	 * @param query = la requête à vérifier
	 * @return un  booléen, true si la requête est valide, false si KO
	 */
	public boolean checkUserInsertInto(String query) {
		Pattern insertInto = Pattern.compile("^(INSERT INTO) ([A-Za-z0-9]+) (VALUES)(\\('[\\w ]{1,25}'(,'[\\w]{1,25}')*\\));$");
		Matcher matchInsertInto = insertInto.matcher(query);
		while (matchInsertInto.find()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Vérifie la validité de la requête
	 * @param query
	 * @return
	 */
	public boolean checkUserUpdateSet(String query) {
		Pattern insertInto = Pattern.compile("^(UPDATE) ([A-Za-z0-9]+) (SET) ([\\w ]{1,25})=('[\\w]{1,25}');$");
		Matcher matchInsertInto = insertInto.matcher(query);
		while (matchInsertInto.find()) {
			return true;
		}
		return false;
	}
	
	/**
	 * @author Thomas
	 * Capture des valeurs entrées dans le INSERT INTO
	 */
	public String captureInsertValues(String query) {
		String data = new String();
		Pattern insertInto = Pattern.compile("(?i)^(INSERT INTO)(?-i) ([A-Za-z0-9]+) (VALUES)(\\('[\\w ]{1,25}'(,'[\\w]{1,25}')*\\));$");
		Matcher matchInsertInto = insertInto.matcher(query);
		while (matchInsertInto.find()) {
			System.out.println("Valeurs insérées capturées!!");
			data = matchInsertInto.group(4);
			System.out.println(data);
		}
		return data;
	}
	
	
	/**
	 * Vérification de la validité de la requête insert into.
	 * 
	 * @param query : la requête à vérifier
	 * @return booléenne : valide ou pas la requête écrite par l'utilisateur
	 * @author Cécile
	 */
	public boolean checkUserInsert(String query) {
		Pattern insert = Pattern
				.compile("^(INSERT INTO) ([\\w]+) VALUES\\([\\w ]{1,25}(, [\\w ]{0,25})*\\);$");
		Matcher insertMatch = insert.matcher(query);
		if (insertMatch.find()) {
			return true;
		}
		return false;
	}
	
	/**
	 * Vérification de la validité de la requête Select all
	 * 
	 * @param query : la requête à vérifier
	 * @return booléenne : valide ou pas la requête écrite par l'utilisateur
	 * @author Cécile
	 */
	public boolean checkUserSelectAll(String query) {
		Pattern insert = Pattern
				.compile("^(SELECT \\* FROM) ([\\w]+);$");
		Matcher insertMatch = insert.matcher(query);
		while (insertMatch.find()) {
			return true;
		}
		return false;
	}

	/**
	 * Permet de récupérer le nom de la table visée par la requête utilisateur
	 * 
	 * @param query : la requête de l'utilisateur
	 * @return le nom de la table visée
	 * @author Cécile
	 */
	public String captureCreateTableName(String query) {
		// todo : put variable into regex ?
		String name = new String();
		Pattern tableName = Pattern.compile(
				"^(CREATE TABLE) ([\\w]+)\\([\\w ]{1,25}(, [\\w ]{0,25})*\\);$");
		Matcher matchTableName = tableName.matcher(query);
		while (matchTableName.find()) {
			name = matchTableName.group(2);
			return name;
		}
		return name;
	}
	/**
	 * @author Thomas
	 * Méthode de récupération du nom de la table visée par la requête update
	 * @return table visée
	 * @param query : la requête à traiter
	 */
	public String captureUpdateTableName(String query) {
		String name = new String();
		Pattern tableName = Pattern.compile("^(UPDATE) ([\\w]+) (SET) (([\\w ]{0,25})*)='([\\w ]{1,25})';$");
		Matcher matchTableName = tableName.matcher(query);
		while(matchTableName.find()) {
			name = matchTableName.group(2);
			return name;
		}
		return name;
	}
	/**
	 * @author Thomas
	 * Méthode pour trouver la colonne à modifier dans la requête update
	 * @param query : la reque^te à traiter
	 * @return colonne à modifier
	 */
	public String captureColumnName(String query) {
		String column = new String();
		Pattern columnName = Pattern.compile("^(UPDATE) ([\\w]+) (SET) (([\\w ]{0,25})*)='([\\w ]{1,25})';$");
		Matcher matchColumnName = columnName.matcher(query);
		while(matchColumnName.find()) {
			column = matchColumnName.group(4);
			return column;
		}
		return column;
	}
	
	/**
	 * @author Thomas
	 * Méthode pour récupérer la nouvelle valeur qui sera à écrire dans toute la colonne désirée
	 * @param query : la requête à traiter
	 * @return newValue : la nouvelle valeur qui sera utilisée pour l'update
	 */
	public String captureNewValue(String query) {
		String newValue = new String();
		Pattern pattNewValue = Pattern.compile("^(UPDATE) ([\\w]+) (SET) (([\\w ]{0,25})*)='([\\w ]{1,25})';$");
		Matcher matchNewValue = pattNewValue.matcher(query);
		while(matchNewValue.find()) {
			newValue = matchNewValue.group(6);
			return newValue;
		}
		return newValue;
	}
	
	
	/**
	 * Permet d'obtenir le nom de table d'après la requête Select all from
	 * 
	 * @param query : la requête à traiter
	 * @return name : le nom de la table à traiter
	 * @author Thomas
	 */
	public String captureSelectAllTableName(String query) {
		String name = new String();
		Pattern tableName = Pattern.compile("^(SELECT \\* FROM) ([\\w]+);$");
		Matcher matchTableName = tableName.matcher(query);
		while(matchTableName.find()) {
			name = matchTableName.group(2);
			return name;
		}
		return name;
	}
	
	/**
	 * Permet d'obtenir les valeurs pour un select all
	 * @param query : la requête à traiter
	 * @return value
	 */
	public String captureSelectAllValues(String query) {
		String value = new String();
		Pattern valueMatch = Pattern.compile("^(SELECT \\* FROM) ([\\w]+);$");
		Matcher match = valueMatch.matcher(query);
		while(match.find()) {
			value = match.group(2);
			return value;
		}
		return value;
	}

	/**
	 * Permet de récupérer le nom de la table visée par la requête insert into de
	 * l'utilisateur
	 * 
	 * @param query : la requête de l'utilisateur
	 * @return le nom de la table visée
	 * @author Cécile
	 */
	public String captureInsertTableName(String query) {
		String name = new String();
		Pattern tableName = Pattern.compile("^(INSERT INTO) ([\\w]+) VALUES\\('[\\w ]{1,25}'(,'[\\w ]{0,25}')*\\);$");
		Matcher matchTableName = tableName.matcher(query);
		while (matchTableName.find()) {
			name = matchTableName.group(2);
			return name;
		}
		return name;
	}

	/**
	 * Permet de récupérer le nom des valeurs/champs
	 * 
	 * @param query : la requête de l'utilisateur
	 * @return le nom de la table visée
	 * @author Cécile
	 */
	public String captureValues(String query) {
		Pattern values = Pattern.compile("(\\(([\\w ]{1,25})(, [\\w ]{0,25})*\\));$");
		Matcher matchTableName = values.matcher(query);

		String value = new String();
		while (matchTableName.find()) {
			value = matchTableName.group(0);
			return value;
		}
		return value;
	}

	/**
	 * Ouverture du fichier config.ini dans C:/ENV/BDD afin de récupérer login/mdp
	 * 
	 * @author Thomas
	 * 
	 */
	public String loginCollect() {
		FileReader fr = null;
		BufferedReader br = null;
		String iniVal = "";

		try {
			fr = new FileReader("C:/ENV/BDD/config.ini");
			br = new BufferedReader(fr);
			iniVal = br.readLine();
		} catch (Exception e) {

		} finally {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// System.out.println(iniVal);
		return iniVal;
	}

	/**
	 * Vérification du login/mdp
	 * 
	 * @param userAnswer : la réponse de l'utilisateur sur l'écran de login
	 * @param iniVal     : les valeurs du fichier config.ini (ici, les données du
	 *                   compte utilisateur)
	 * 
	 * @author Thomas
	 */
	public boolean checkIdentity(String userAnswer, String iniVal) {
		String[] logPass = iniVal.split("/");
		String log = logPass[0];
		String pass = logPass[1];

		String[] userLogPass = userAnswer.split("/");
		String userLog = userLogPass[0];
		String userPass = userLogPass[1];

		if (userLog.equals(log) && userPass.equals(pass)) {
			return true;
		} else {
			System.out.println("/!\\ Identification incorrecte /!\\\n");
		}
		return false;
	}

	/**
	 * @author Thomas Méthode qui permet de vérifier si la table avec laquelle on
	 *         souhaite interagir est existante
	 * @return booléen true si le fichier existe, le programme continue. Retourne
	 *         message d'erreur si la table n'existe pas
	 */
	public boolean checkTableExists(String tableName) {

		File cda = new File("C:/ENV/BDD/" + tableName + ".cda");
		if (cda.exists() && cda.isFile()) {
			return true;
		} else {
			System.out.println("Erreur : la table demandée n'existe pas.");
		}
		return false;
	}

}
